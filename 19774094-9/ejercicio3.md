# Ejercicio 3 

__1. Cantidad de Branches__

* Cantidad de Branches: __1.__
* Nombre: __master.__

__2. Cantidad de Tags__
	
* Cantidad: __0.__

__3. Nombre, Mensaje y código hash de ultimos 3 commit__

 __a. Commit:__ 218dffa6d013e78c42620d7dbfaf9786c14a9e27

 * __Autor:__ Karina Bernal <karina.bernalb2015@gmail.com>
 * __Fecha:__ Fri Aug 24 21:01:48 2018 -0300
 * __Mensaje:__ Agrega datos.md

 __b. Commit:__ 0bf6860c1d2d7128056d639354141fdbab6bfe75

 * __Autor:__ Karina Bernal <karina.bernalb2015@gmail.com> 
 * __Fecha:__ Fri Aug 24 20:58:11 2018 -0300
 * __Mensaje:__ Agrega ejercicio 3
 
  __c. Commit:__ 98d528d0674359cdfd671ec58641a6042db8dd53

 * __Autor:__ Karina Bernal <karina.bernalb2015@gmail.com>
 * __Fecha:__ Fri Aug 24 20:44:24 2018 -0300
 * __Mensaje:__ Agrega glosario
